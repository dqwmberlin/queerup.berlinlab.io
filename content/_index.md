**Aus persönlichen Gründen passt der Termin Donnerstagabend leider nicht mehr - wir überlegen uns was Neues und sagen euch dann Bescheid! <3**

**Frohes Schreiben!** 

*****

Hey, wir sind die Disaster Queers - ein bisschen kreativ-chaotisch, ein bisschen selbstironisch, auf jeden Fall aber: eine Gruppe leidenschaftlicher Autor*innen ohne cis Männer, die sich gerne untereinander austauschen und gegenseitig motivieren!

Wir treffen uns jeden zweiten Donnerstag im Monat von 16:00 bis 22:00 Uhr in Berlin (natürlich kannst du einfach jederzeit noch später dazukommen oder früher gehen). 

*****

Du passt vielleicht zu uns, wenn du...

...keine Lust hast, zum tausendsten Mal zu diskutieren, dass Rassismus existiert.  

...so viele schlechte Erfahrungen mit cis Männern gemacht hast, dass du einfach mal deine Ruhe vor ihnen haben möchtest.  

...trans* bist und dich darauf verlassen können willst, mit deinem Namen und deinem Pronomen angesprochen zu werden.  

...nichtbinär oder intergeschlechtlich bist oder kein Geschlecht hast und dir nicht anhören müssen willst, dass es das doch gar nicht gäbe.  

...bisexuell, pansexuell und/oder anderweitig nicht-monosexuell bist und die Mythen und Vorurteile darum herum satt hast.  

...asexuell, aromantisch bist und/oder polyamor lebst und Geschichten jenseits der immer gleichen Romantischen Zweierbeziehungen entdecken möchtest. 

...gerne unter Leuten sein möchtest, es dir aber nicht leisten kannst, ständig Geld im Café auszugeben.  

...körperliche oder geistige Be_hinderungen hast und dich darauf verlassen können möchtest, dass darauf eingegangen wird.  

...dir niemals zutrauen würdest, ein ganzes Buch zu schreiben oder gar zu veröffentlichen - aber dran bleiben möchtest, einfach für dich zu schreiben, weil es dir Spaß macht.  

...dich auf dem Weg ins berufliche Schreiben vernetzen möchtest.  

...Schwierigkeiten mit Grammatik und Rechtschreibung hast und willst, dass deine Geschichten angehört werden, statt sich über deine Formschwächen lustig zu machen.  